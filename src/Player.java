/**
 * Небольшой класс, описывающий игрока.
 * @author ITA, доработка - Малякин Кирилл
 */
public class Player {

    private boolean guessed;

    /**
     * Возвращает целое число, которое загадал игрок.
     * @param target целевое число для проверки, угадал ли игрок.
     * @return число, которое загадал игрок.
     */
    //Тавтология в комментариях - наше всё. Как, впрочем, и самоирония.
    public int guess(int target) {
        int number = (int) (Math.random() * 10);
        //System.out.println("I'm guessing " + number);
        guessed = number == target;
        return number;
    }

    public boolean isGuessed() {
        return guessed;
    }
}
