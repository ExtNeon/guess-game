﻿/**
 * Класс с лаунчером. Занимается исключительно запуском игры.
 * @author ITA, доработка - Малякин Кирилл
 */
public class GameLauncher {
    public static void main (String[] args) {
        new GuessGame().startGame(); // Незачем тут объект городить.
    }
}
