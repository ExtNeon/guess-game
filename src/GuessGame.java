/**
 * В этом классе содержится метод, в котором реализована практически вся логика игры.
 * Поля класса содержат сервисные данные: локализацию и количество игроков.
 * @author ITA, доработка - Малякин Кирилл
 */
public class GuessGame {

    private static final int playersCount = 3;

    private static final String phrase1 = "Я загадываю число, находящееся в пределах от 0 до 9"; //"I'm thinking of a number between 0 and 9..."
    private static final String phrase2 = "Целевое число для отгадывания - "; //"Number to guess is "
    private static final String phrase3 = " игрок загадал "; //" player guessed ";
    private static final String phrase4 = "У нас есть победитель!"; //"We have a winner!"
    private static final String phrase5 = "Игроки попытаются угадать ещё раз.";//"Players will have to try again."
    private static final String phrasePlayer = " игрок ";
    private static final String phraseGuessed = "угадал!";
    private static final String phraseFailed = "потерпел неудачу.";
    private static final String phraseGameOver = "Игра окончена";

    /**
     * Метод, запускающий игру и содержащий её алгоритм.
     */
   public void startGame() {

       Player players[] = new Player[playersCount]; //Тут мы просто говорим, сколько элементов в массиве
       //Здравствуй, школа. Деградация + 100%
       int targetNumber = (int) (Math.random() * 10);
       System.out.println(phrase1);
       for (int i = 0; i < playersCount; i++) {
           players[i] = new Player(); //А вот тут мы уже создаём объекты.
       }
       boolean weHaveAWinner = false;
       while(true) { //Цикл будет повторяться до тех пор, пока не выявится победитель.
           System.out.println(phrase2 + targetNumber);
           for (int i = 0; i < playersCount; i++) {
               System.out.println((i+1) + phrase3 + players[i].guess(targetNumber));
               weHaveAWinner = weHaveAWinner || players[i].isGuessed(); //Тут мы проверяем, есть ли вообще счастливчик
           }
           if (weHaveAWinner)
           {
               System.out.println(phrase4);
               for (int i = 0; i < playersCount; i++) {
                   System.out.println((i + 1) + phrasePlayer + (players[i].isGuessed() ? phraseGuessed : phraseFailed));
               }
               System.out.println(phraseGameOver);
               break;
           }
           else
           {
               System.out.println(phrase5);
           }
       }
   }
}
